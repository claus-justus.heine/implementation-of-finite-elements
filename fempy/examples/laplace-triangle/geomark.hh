#include <iostream>

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/function/common/rangegenerators.hh>
#include <dune/fem/function/common/localcontribution.hh>
#include <dune/fem/function/localfunction/const.hh>
#include <dune/fem/gridpart/common/entitysearch.hh>
#include <dune/fem/common/bindguard.hh>

/**Mark elements for refinement which intersect a given disc.
 *
 * This variant sets the estimator value to 1 for those elements
 * (otherwise to zero) and relies on the python marking routine
 *
 * dune.fem.mark(estimate, 0.5, 0.1)
 */
template<class Estimator, class Point, class Radius>
void estimateInsideDisk(Estimator &estimator, const Point &midPoint, const Radius &radius)
{
  // Access the local DoF for one element.
  Dune::Fem::SetLocalContribution<Estimator> estimatorLocal(estimator);

  // Loop over all elements.
  for (const auto& entity : Dune::Fem::entities(estimator)) {    
    // Bind to the current element
    auto guard = Dune::Fem::bindGuard(estimatorLocal, entity);

    // Default set to 0 in order not to refine non-intersecting elements
    estimatorLocal[0] = 0.0;
    
    // Get the geometry of the current element in order to access the vertices
    const auto& geometry = entity.geometry();
    
    // Set the value if any of the vertices intersects the given disc
    for (int i = 0; i < geometry.corners(); ++i) {
      // This is the global coordinate
      auto corner = geometry.corner(i);

      if ((corner-midPoint)*(corner-midPoint) <= radius*radius) {
        estimatorLocal[0] = 1.0;
        break; // no need to continue
      }
    }
  }
}

/**Mark elements for refinement which intersect a given disc.
 *
 * This variant directly marks elements for refinement without
 * computing an intermediate "estimate", so calling
 * dune.fem.mark(estimate, ...) in the Python code is not necessary.
 *
 */
template<class GridView, class Point, class Radius>
auto markInsideDisk(GridView& gridView, const Point &midPoint, const Radius &radius)
{
  // Bookkeeping in order to return something to the caller
  int refined = 0;

  // obtain the hierarchical grid from the leaf GridView
  // C++/Dune stuff: unfortunately we cannot obtain a non-const instance
  //                 of the hierarchical grid directly.
  auto& grid = const_cast<typename GridView::Grid&>(gridView.grid());
  
  for(const auto &entity : elements(gridView)) {
    // Get the geometry of the current element in order to access the vertices
    const auto& geometry = entity.geometry();

    // By default mark for coarsening
    grid.mark(-1, entity);

    // Set the value if any of the vertices intersects the given disc
    for (int i = 0; i < geometry.corners(); ++i) {
      // This is the global coordinate
      auto corner = geometry.corner(i);

      if ((corner-midPoint)*(corner-midPoint) <= radius*radius) {
        grid.mark(1, entity);
        ++refined;
        break; // no need to continue
      }
    }
  }
  return std::make_pair(refined, gridView.size(0) - refined);  
}
