import ufl
from ufl import grad, div, dot, dx, ds, inner, sin, cos, pi, exp, sqrt
import dune.ufl
import dune.grid
import dune.fem
import dune.alugrid

# number of global refinements in main loop
refineIterations = 20

dgfFile = "solidsquare.dgf"
#dgfFile = "square-triangle.dgf"
#dgfFile = "hole-handmade.dgf"
hierarchicalGrid = dune.alugrid.aluConformGrid(dgfFile, dimgrid = 2)
gridView = dune.fem.view.adaptiveLeafGridView(hierarchicalGrid)

space = dune.fem.space.lagrange(gridView, order=1)
u     = ufl.TrialFunction(space)
phi   = ufl.TestFunction(space)
x     = ufl.SpatialCoordinate(space)

# define storage for discrete solutions
uh     = space.interpolate(0, name="uh")
uh_old = uh.copy()

# problem definition

K = 1   # diffusion coefficient
c = -1  # reaction coefficient (< 0, so stuff is consumed)
f = 1   # load, i.e. "right hand side"

# space form
diffusiveFlux = K*grad(u)
source = c*u
form = (dot(diffusiveFlux, grad(phi)) + source * phi + f * phi) * dx

# Generate the scheme and prescribe Dirichlet conditions with value 0 and 2 on boundary with id 1 and with id 3
scheme = dune.fem.scheme.galerkin([form == 0, dune.ufl.DirichletBC(space, 0, 1), dune.ufl.DirichletBC(space, 2, 3)], solver="cg")

vtk = gridView.sequencedVTK("data/laplace-triangle", pointdata=[uh, uh_old])
# vtk() uh is just zero.

midPoint = dune.common.FieldVector([-0.2, -0.2])
radius = 0.1

for i in range(refineIterations+1):
    info = scheme.solve(target=uh)

    print("Refine iteration ", i,
          "Solver iterations: ", info["linear_iterations"],
          "#Ent: ", gridView.size(0)
    )
    vtk()
    if i <= refineIterations:
        if True:
            # call the C++ algorithm in order to compute the "estimate"
            [refined, coarsened] = dune.generator.algorithm.run("markInsideDisk", "geomark.hh", gridView, midPoint, radius)
        else:
            # alternatively: do it in Python (less efficient)
            def markInsideDisk(e):
                for coordinate in e.geometry.corners:
                    if (coordinate - midPoint)*(coordinate - midPoint) <= radius * radius:
                        return dune.grid.Marker.refine
                return dune.grid.Marker.coarsen
                #return dune.grid.Marker.keep
            
            [refined, coarsened] = gridView.hierarchicalGrid.mark(markInsideDisk)
        if refined == 0:
            # no element has been marked we refine globally
            print("Marking globally")
            gridView.hierarchicalGrid.mark(lambda e: dune.grid.Marker.refine)
        else:
            print("marking: #refined/coarsended: ", refined, coarsened)
        dune.fem.adapt([uh])
        uh_old.assign(uh)
