REGISTRY=registry.dune-project.org/claus-justus.heine/implementation-of-finite-elements
if [ "$1" != "" ] ; then
  DUNEVERSION="$1"
  docker volume rm dunepy$DUNEVERSION
else
  DUNEVERSION="latest"
  docker volume rm dunepy
fi
dockerName=$REGISTRY:$DUNEVERSION

docker volume rm implfem2020-$DUNEVERSION
docker image rm $dockerName
docker system df

docker build . --no-cache -t $dockerName --build-arg DUNEVERSION="$DUNEVERSION"
