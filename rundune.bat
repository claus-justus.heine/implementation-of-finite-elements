@echo off
set REGISTRY=registry.dune-project.org/claus-justus.heine/implementation-of-finite-elements
set BASE=implfem2020
REM just something AFAIK
set USERID=4711
set GROUPID=%USERID%

if "%1" == "latest" (
   set DUNEVERSION=latest
) else (
  if "%1" == "" (
    set DUNEVERSION=2.7
  ) else (
    set DUNEVERSION=%1
    )
)

set dockerName=%REGISTRY%:%DUNEVERSION%
set CONTAINER=%BASE%-%DUNEVERSION%

for /f "usebackq tokens=*" %%a in (`docker -v`) do set "DOCKERVERSION=%%a"

if "x%DOCKERVERSION:Docker=%" == "x%DOCKERVERSION%" (
  echo.
  echo Docker could not be found on your system.
  echo Please install docker following instructions for you operating system from
  echo https://docs.docker.com.
  exit 
)

for /f "delims=[] tokens=2" %%a in ('ping -4 -n 1 %ComputerName% ^| findstr [') do set IPV4ADDR=%%a

docker container inspect %CONTAINER% > $null 2>&1
if ERRORLEVEL 1 (
  REM https://dev.to/darksmile92/run-gui-app-in-linux-docker-container-on-windows-host-4kde
  echo.
  echo For X forwarding you will need an X-server app running.
  echo Use for example "vcxsrv", start it with "xlaunch" from
  echo the Windows start-menu and tick "disable access control"
  echo.
  docker run -it --rm -p 8888:8888 -v %cd%:/host -v %BASE%-%DUNEVERSION%:/dunepy --name=%BASE%-%DUNEVERSION% -e DISPLAY=%IPV4ADDR%:0 --privileged -e userId=%USERID% -e groupId=%GROUPID% %REGISTRY%:%DUNEVERSION%
) else (
  docker start -i %CONTAINER%
)
