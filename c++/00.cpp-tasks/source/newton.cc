#include <iostream>
#include <fstream>
#include <cmath>
#include <memory>

double f(double x)
{
  //return std::atan(10.*x) + std::sin(10.*M_PI*x) - x/10.;
  return x*x - 2.0;
}
double df(double x)
{
  double dfx = 0;
  /*********************************************************************
   ***   TODO: Implement f'(x)                                       ***
   *********************************************************************/
  return dfx;
}

std::pair< double, int >
newton( double a,double b, double eps )
{
  if( a > b )
    return newton( b, a, eps );

  double fa = f(a);
  double fb = f(b);
  // start value
  double z = 0.5*(a+b);
  if (std::abs(fa)<eps)
  {
    return std::make_pair( a, 0 );
  }
  if (std::abs(fb)<eps)
  {
    return std::make_pair( b, 0 );
  }
  if (fa*fb>0)
  {
    std::cerr << "Wrong input parameters!" << "f(a)*f(b) = " << fa*fb << std::endl;
    return std::make_pair( 0.0, -1 );
  }
  int counter = 0 ;
  /*********************************************************************
   ***   TODO: Implement Newton's methods:                           ***
   ***                                                               ***
   ***         For given x_0 and k>=0                                ***
   ***                                                               ***
   ***         x_k+1 = x_k - f( x ) / f'(x)                          ***
   ***                                                               ***
   ***         Stop the iteration when | x_k+1 - x_k | < eps         ***
   ***                                                               ***
   *********************************************************************/
  return std::make_pair( z, counter );
}

int main()
{
  double TOL = 1.0e-12;
  double a = -0., b = 3.;
  std::cout.precision(20); std::cout << std::fixed;
  auto x = newton(a,b,TOL);
  std::cout << "Newton: f( " << x.first << " ) == 0     (iterations = " << x.second << ")" << std::endl << std::endl;

  std::cout << "Check: f( " << x.first << " ) = " << f( x.first ) << std::endl;
}

