#ifndef DUNE_TRILINEARMAPPING_HH
#define DUNE_TRILINEARMAPPING_HH

// System includes
#include <limits>
#include <cmath>

// Dune includes
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

namespace Dune {

  //! A trilinear mapping from the Dune reference hexahedron into the physical
  //! space.
  class TrilinearMapping
  {
  public:
    typedef FieldVector<double, 3>    GlobalCoordinate;
    typedef FieldVector<double, 3>    LocalCoordinate;

    typedef FieldMatrix<double, 3, 3> Jacobian;

    typedef FieldMatrix< double, 8, 3 > MappingMatrix;
  private:
    // the internal mapping
    MappingMatrix A_;

    void DF(const double, const double, const double) ;
    void inverse (const LocalCoordinate&) ;
  public :
    TrilinearMapping (const MappingMatrix& mat)
      : TrilinearMapping( mat[ 0 ], mat[ 1 ], mat[ 2 ], mat[ 3 ],
                          mat[ 4 ], mat[ 5 ], mat[ 6 ], mat[ 7 ])
    {}

    TrilinearMapping (const GlobalCoordinate& p0, const GlobalCoordinate& p1,
                      const GlobalCoordinate& p2, const GlobalCoordinate& p3,
                      const GlobalCoordinate& p4, const GlobalCoordinate& p5,
                      const GlobalCoordinate& p6, const GlobalCoordinate& p7)
    {
      buildMapping( p0, p1, p2, p3, p4, p5, p6, p7 );
    }

    template <class vector_t>
    void buildMapping(const vector_t& p0, const vector_t& p1,
                      const vector_t& p2, const vector_t& p3,
                      const vector_t& p4, const vector_t& p5,
                      const vector_t& p6, const vector_t& p7)
    {
      // build mapping
      A_[ 0 ] = p0;

      A_[ 1 ] = p1 - p0;

      A_[ 2 ] = p2 - p0;

      A_[ 3 ] = p4 - p0;

      A_[ 4 ] = p3 - p2 - A_[ 1 ];

      A_[ 5 ] = p6 - p4 - A_[ 2 ];

      A_[ 6 ] = p5 - p1 - A_[ 3 ];

      A_[ 7 ] = p7 - p5 + p4 - p6 - p3 + p1 + A_[ 2 ];
    }

    GlobalCoordinate global( const LocalCoordinate& local ) const
    {
      const double x = local[ 0 ];
      const double y = local[ 1 ];
      const double z = local[ 2 ];

      const double xy  = x * y ;
      const double yz  = y * z ;
      const double xz  = x * z ;
      const double xyz = x * yz ;

      // F( x, y, z )^T = a_0 + x * a_1 + y * a_2 + z * a_3 + xy * a_4 + yz * a_5 + xz * a_6 + xyz * a_7
      GlobalCoordinate global;

      global = A_[ 0 ] ;
      global.axpy( x,   A_[ 1 ] );
      global.axpy( y,   A_[ 2 ] );
      global.axpy( z,   A_[ 3 ] );
      global.axpy( xy,  A_[ 4 ] );
      global.axpy( yz,  A_[ 5 ] );
      global.axpy( xz,  A_[ 6 ] );
      global.axpy( xyz, A_[ 7 ] );
      return global;
    }

    void transpose( Jacobian& jac ) const
    {
      Jacobian jacT( jac );
      for( int i=0; i<3; ++i )
        for( int j=0; j<3; ++j )
          jac[ i ][ j ] = jacT[ j ][ i ];
    }

    Jacobian jacobian( const LocalCoordinate& local ) const
    {
      const double x = local[ 0 ];
      const double y = local[ 1 ];
      const double z = local[ 2 ];

      const double xy  = x * y ;
      const double yz  = y * z ;
      const double xz  = x * z ;

      Jacobian jac( 0 );

      // NOTE: this is the transposed Jacobian matrix
      // DF_x( x,y,z )^T = a_1 + y * a_4 + z * a_6 + yz * a_7
      // DF_y( x,y,z )^T = a_2 + x * a_4 + z * a_5 + xz * a_7
      // DF_z( x,y,z )^T = a_3 + y * a_5 + x * a_6 + xy * a_7

      jac[ 0 ] = A_[ 1 ];
      jac[ 0 ].axpy( y,  A_[ 4 ] );
      jac[ 0 ].axpy( z,  A_[ 6 ] );
      jac[ 0 ].axpy( yz, A_[ 7 ] );

      jac[ 1 ] = A_[ 2 ];
      jac[ 1 ].axpy( x,  A_[ 4 ] );
      jac[ 1 ].axpy( z,  A_[ 5 ] );
      jac[ 1 ].axpy( xz, A_[ 7 ] );

      jac[ 2 ] = A_[ 3 ];
      jac[ 2 ].axpy( y,  A_[ 5 ] );
      jac[ 2 ].axpy( z,  A_[ 6 ] );
      jac[ 2 ].axpy( xy, A_[ 7 ] );
      transpose( jac );
      return jac;
    }

    LocalCoordinate local( const GlobalCoordinate& x ) const
    {
      LocalCoordinate w( 0.5 );

      int count = 0;

      GlobalCoordinate m( 0 );
      GlobalCoordinate delta( 0 );

      // We define the mapping
      // G( w ) = F( w ) - x
      // and we want to search w0 such that
      // G( w0 ) = 0
      // We will use Newton's method for that

/*********************************************************/
/***                 NEW FOR LESSON 3                  ***/
/*********************************************************/
      const bool useInverse = false ;
      if( useInverse )
/*********************************************************/

      {
        do
        {
          Jacobian jac = jacobian( w );
          jac.invert();

          m = global( w );
          m -= x;

          jac.mv( m, delta );

          w -= delta;
          ++count ;
          if( count > 100 )
          {
            std::cout << "Newton iteration diverged!"<<std::endl;
            break;
          }
        }
        while ( delta.two_norm() > 1e-8 );
      }

/*********************************************************/
/***                 NEW FOR LESSON 3                  ***/
/*********************************************************/
      else
      {
      /****************************************************************
       ***                                                          ***
       *** TODO: Implement Newton iteration to find  G( w ) = 0     ***
       ***       This time, try to avoid inversion of matrices      ***
       ***                                                          ***
       *** Hint: implement newton iteration avoiding inversion      ***
       ***       of matrices.                                       ***
       ***       Obviously: DG == DF and therefore DG^-1 == DF^-1   ***
       ***       So we can write:                                   ***
       ***         w_k+1 = w_k - DF^-1( w_k ) * G( w_k )            ***
       ***         where we need to invert DF, or we write          ***
       ***         DF( w_k+1 ) = DF w_k - G( w_k )                  ***
       ****************************************************************/
      }
/*********************************************************/

      std::cout << "Iterations used: " << count << std::endl;
      return w;
    }

  };

} // end namespace Dune

#endif
