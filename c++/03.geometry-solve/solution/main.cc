/** always start a dune main file by including config.hh **/
#include <config.h>

/** standard headers **/
#include<iostream>

#include "linear.hh"

#include "trilinearmapping.hh"

int main ()
{
  {
    typedef typename Dune::LinearMapping::GlobalCoordinate GlobalCoordinate;
    typedef typename Dune::LinearMapping::GlobalCoordinate LocalCoordinate;

    GlobalCoordinate p0( {{ -0.7, -1, 8 }} );
    GlobalCoordinate p1( {{ -1.2, -1, 8 }} );
    GlobalCoordinate p2( {{ -0.494975, -1.49497, 7.375 }} );
    GlobalCoordinate p3( {{ -0.7, -1, 8.5 }} );

    // reference tetrahedron
    /*
    GlobalCoordinate p0( {{ 0, 0, 0 }} );
    GlobalCoordinate p1( {{ 1, 0, 0 }} );
    GlobalCoordinate p2( {{ 0, 1, 0 }} );
    GlobalCoordinate p3( {{ 0, 0, 1 }} );
    */

    Dune::LinearMapping map( p0, p1, p2, p3 );

    LocalCoordinate local( 0.25 );
    auto center = map.global( local );

    std::cout << center << std::endl;

    auto localCenter = map.local( center );
    std::cout << localCenter << std::endl;
  }

  {
    typedef typename Dune::TrilinearMapping::GlobalCoordinate GlobalCoordinate;
    typedef typename Dune::TrilinearMapping::GlobalCoordinate LocalCoordinate;

    // 0 20 23 24 31 32 37 38
    //
    //
    // P0:  -0.7 -1 8
    // P1:  -1.2 -1 8
    // P2:  -0.494975 -1.49497 7.375
    // P3:  -0.848528 -1.84853 7.375
    // P4:  -0.7 -1 8.5
    // P5:  -1.2 -1 8.5
    // P6:  -0.494975 -1.49497 7.875
    // P7:  -0.848528 -1.84853 7.875
    //
    GlobalCoordinate p0( {{ -0.7, -1, 8 }} );
    GlobalCoordinate p1( {{ -1.2, -1, 8 }} );
    GlobalCoordinate p2( {{ -0.494975, -1.49497, 7.375 }} );
    GlobalCoordinate p3( {{ -0.848528, -1.84853, 7.375 }} );
    GlobalCoordinate p4( {{ -0.7, -1, 8.5 }} );
    GlobalCoordinate p5( {{ -1.2, -1, 8.5 }} );
    GlobalCoordinate p6( {{ -0.494975, -1.49497, 7.875 }} );
    GlobalCoordinate p7( {{ -0.848528, -1.84853, 7.875 }} );

    // reference cube
    /*
    GlobalCoordinate p0( {{ 0, 0, 0 }} );
    GlobalCoordinate p1( {{ 1, 0, 0 }} );
    GlobalCoordinate p2( {{ 0, 1, 0 }} );
    GlobalCoordinate p3( {{ 1, 1, 0 }} );
    GlobalCoordinate p4( {{ 0, 0, 1 }} );
    GlobalCoordinate p5( {{ 1, 0, 1 }} );
    GlobalCoordinate p6( {{ 0, 1, 1 }} );
    GlobalCoordinate p7( {{ 1, 1, 1 }} );
    */

    Dune::TrilinearMapping map( p0, p1, p2, p3, p4, p5, p6, p7 );

    // TODO: test with corners of reference cube

    LocalCoordinate localCenter( 0.3 );
    auto center = map.global( localCenter );

    GlobalCoordinate centerCheck;
    std::cout << center << std::endl;

    auto invCenter = map.local( center );
    std::cout << invCenter << std::endl;
  }
  return 0;
}
