#ifndef DUNE_LINEARMAPPING_HH
#define DUNE_LINEARMAPPING_HH

// System includes
#include <limits>
#include <cmath>

// Dune includes
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

namespace Dune {

  //! A linear mapping from the Dune reference tetrahedron into the physical
  //! space.
  class LinearMapping
  {
  public:
    typedef FieldVector<double, 3>      GlobalCoordinate;
    typedef FieldVector<double, 3>      LocalCoordinate;

    typedef FieldMatrix<double, 3, 3>   JacobianTransposed;
    typedef FieldMatrix<double, 3, 3>   JacobianInverseTransposed;

    typedef FieldMatrix<double, 4, 3>   MappingMatrix;
  protected:
    GlobalCoordinate p0_;

    // the internal mapping
    JacobianTransposed A_;

  public :
    LinearMapping (const MappingMatrix& mat)
      : LinearMapping( mat[ 0 ], mat[ 1 ], mat[ 2 ], mat[ 3 ] )
    {}

    LinearMapping (const GlobalCoordinate& p0, const GlobalCoordinate& p1,
                   const GlobalCoordinate& p2, const GlobalCoordinate& p3)
    {
      buildMapping( p0, p1, p2, p3 );
    }

    template <class vector_t>
    void buildMapping(const vector_t& p0, const vector_t& p1,
                      const vector_t& p2, const vector_t& p3 )
    {
      p0_ = p0;

      // build Jacobian (transposed)
      A_[ 0 ]  = p1;
      A_[ 0 ] -= p0 ;

      A_[ 1 ]  = p2;
      A_[ 1 ] -= p0;

      A_[ 2 ]  = p3;
      A_[ 2 ] -= p0;
    }

    const JacobianTransposed& jacobian( const LocalCoordinate& local ) const
    {
      return A_;
    }

    // implements F( lcl ) = A * lcl + p_0
    GlobalCoordinate global( const LocalCoordinate& lcl ) const
    {
      GlobalCoordinate glb;
      // glb = F( lcl ) = A * lcl + p_0
      glb = p0_ ;

      // glb += A^T * lcl
      A_.umtv( lcl, glb );
      return glb;
    }

    LocalCoordinate local( const GlobalCoordinate& glb ) const
    {
      // copy matrix before inversion
      JacobianInverseTransposed inv( A_ );

      LocalCoordinate lcl( 0 );
      // compute pt = glb - p0
      GlobalCoordinate pt = glb;
      pt -= p0_;

      // compute inverse of A
      inv.invert();

      // apply inverse (transposed)
      // lcl = A^-T * (glb - p0)
      inv.mtv( pt, lcl );
      return lcl;
    }

  };

} // end namespace Dune

#endif
