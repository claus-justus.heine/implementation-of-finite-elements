#include <config.h>

/** standard headers **/
#include<iostream>

/** dune headers **/
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>
#include<dune/common/typetraits.hh>

/*****************************************
 * Start with some interface classes
*****************************************/

template< int dim, class Impl >
struct C0Function
{
  /** export dimension of domain **/
  static const int dimension = dim;

  /** single coordinate type **/
  typedef double ctype;

  /** argument type **/
  typedef Dune::FieldVector< ctype, dimension > DomainType;

  /** range type **/
  typedef Dune::FieldVector< ctype, dimension > RangeType;

  /** evaluate **/
  void evaluate ( const DomainType & x, RangeType & y ) const
  {
    asImp().evaluate( x, y );
  }

protected:
  /** constructor **/
  C0Function () { }

  /** copy constructor **/
  C0Function ( const C0Function & ) { }

  /** cast to implementation **/
  const Impl & asImp () const { return static_cast< const Impl & > (*this); }
};

/****************************/
/** DifferentiableFunction **/
/****************************/

template< int dim, class Impl >
class C1Function
: public C0Function< dim, Impl >
{
  /** type of base class **/
  typedef C0Function< dim, Impl > BaseType;

public:

  /** type of jacobian **/
  typedef Dune::FieldMatrix< typename BaseType::ctype, BaseType::dimension, BaseType::dimension > JacobianType;

  /** jacobian **/
  void jacobian ( const typename  BaseType::DomainType & x, JacobianType & j ) const
  {
    asImp().jacobian( x, j );
  }

protected:
  using BaseType::asImp;
};

template< class F >
struct AutomaticDiff : public C1Function<F::dimension,AutomaticDiff<F> >
{
  /****************************************************************
   *** TODO: implement a method that fills the jacobian         ***
   ***       using a finite-difference approximation            ***
   ****************************************************************/
};

/************************************************************
 * Implement a few functions derived from the interfaces
*************************************************************/

struct F0 : public C0Function< 2,F0 >
{
  typedef C0Function<2,F0> BaseType;
  typedef BaseType::DomainType DomainType;
  typedef BaseType::RangeType RangeType;

  void evaluate ( const DomainType & x, RangeType & y ) const
  {
    y[ 0 ] = -cos( x[ 0 ] ) + x[ 1 ];
    y[ 1 ] = 4.*x[ 0 ]      + x[ 1 ]*x[ 1 ];
  }
};
struct F1 : public C1Function< 2,F1 >
{
  typedef C1Function<2,F1> BaseType;
  // static const int dimension = BaseType::dimension;
  typedef BaseType::DomainType DomainType;
  typedef BaseType::RangeType RangeType;
  typedef BaseType::JacobianType JacobianType;

  void evaluate ( const DomainType & x, RangeType & y ) const
  {
    y[ 0 ] = -cos( x[ 0 ] ) + x[ 1 ];
    y[ 1 ] = 4.*x[ 0 ]      + x[ 1 ]*x[ 1 ];
  }
  /** jacobian **/
  void jacobian ( const DomainType & x, JacobianType & j ) const
  {
    j[ 0 ][ 0 ] = sin( x[ 0 ] );
    j[ 0 ][ 1 ] = 1.;
    j[ 1 ][ 0 ] = 4.;
    j[ 1 ][ 1 ] = 2.*x[ 1 ];
  }
};
struct A1 : public C1Function< 2,A1 >
{
  typedef C1Function<2,A1> BaseType;
  typedef BaseType::DomainType DomainType;
  typedef BaseType::RangeType RangeType;
  typedef BaseType::JacobianType JacobianType;

  A1()
  {
    j_[ 0 ][ 0 ] = 1.;
    j_[ 0 ][ 1 ] = 1.;
    j_[ 1 ][ 0 ] = 4.;
    j_[ 1 ][ 1 ] = 2.;
  }

  void evaluate ( const DomainType & x, RangeType & y ) const
  {
    j_.mv( x, y);
  }
  /** jacobian **/
  void jacobian ( const DomainType & x, JacobianType & j ) const
  {
    j = j_;
  }
  private:
  JacobianType j_;
};

/**************************************************
 * Invert a C1 function using Newton's method
**************************************************/

template <class F>
struct Inverse : public C0Function< F::dimension, Inverse<F> >
{
  typedef C0Function< F::dimension,Inverse<F> > BaseType;
  typedef F Function;

  static const int dimension = Function::dimension;
  typedef typename BaseType::DomainType RangeType;
  typedef typename BaseType::RangeType DomainType;
  Inverse(const Function &f)
  : f_(f)
  {
  }
  void evaluate ( const DomainType & y, RangeType & x ) const
  {
    /** square of fixed tolerance **/
    const double eps2 = 1e-14;

    typename Function::JacobianType df;
    DomainType fx, upd;

    double res2;

    int counter = 0;
    do
    {
      counter++;
      /** calc residuum **/
      f_.evaluate( x, fx );
      fx -= y;
      res2 = fx.two_norm2();
      std::cout << "       iteration: " << counter << " , x: " << x << " , residual^2: " << res2 << std::endl;
      if( counter > 1000 )
      {
        std::cerr << "no convergence in newton method" << std::endl;
        break;
      }

      /** get jacobian **/
      f_.jacobian( x, df );

      /** calc update **/
      df.solve( upd, fx );

      /** apply **/
      x -= upd;
    } while( res2 > eps2 );
  }
  private:
  const Function &f_;
};

template <int dim,class Impl>
void algorithm(const C1Function<dim,Impl> &f)
{
  typedef C1Function<dim,Impl> Function;
  Inverse<Function> invf(f);
  typename Function::RangeType y, z;
  z[ 0 ] =  3.;
  z[ 1 ] = -2.;
  f.evaluate( z, y );
  std::cout << "domain value       z       = " << z << std::endl;
  std::cout << "range value  y = f(z)      = " << y << std::endl;
  typename Function::RangeType x( 0. );
  invf.evaluate(y,x);
  std::cout << "domain value x = f^{-1}(y) = " << x << std::endl;
  std::cout << "error = " << (x - z).two_norm() << std::endl;
}
  /****************************************************************
   *** TODO: write a specialization of algorithm for            ***
   ***       C0Functions using AutomaticDiff                    ***
   ****************************************************************/

int main ( int argc, char ** argv )
{
  {
    std::cout << "use exact jacobian..." << std::endl;
    F1 f;
    algorithm(f);
  }
  /****************************************************************
   *** TODO: call the algorithm for instances of:               ***
   ***       F0, AutomaticDiff<F1> and for the linear           ***
   ***       functions A1.                                      ***
   ***       Determine where our implementation is not          ***
   ***       working efficiently.                               ***
   ****************************************************************/
}

