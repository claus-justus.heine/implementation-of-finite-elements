#ifndef DUNE_FEM_SCHOOL_PROBLEMS_HH
#define DUNE_FEM_SCHOOL_PROBLEMS_HH

/** standard headers **/
#include<iostream>

/** local includes **/
#include "function.hh"

/** forward delcarations **/
template< int, class > class DifferentiableFunction;
template< int, class > class LinearFunction;

class F1
: public DifferentiableFunction< 2, F1 >
{
  typedef DifferentiableFunction< 2, F1 > BaseType;

public:
  /** export dimension of domain **/
  static const int dimension = BaseType::dimension;

  /** single coordinate type **/
  typedef BaseType::ctype ctype;

  /** argument type **/
  typedef BaseType::DomainType DomainType;

  /** range type **/
  typedef BaseType::RangeType RangeType;

  /** type of jacobian **/
  typedef BaseType::JacobianRangeType JacobianRangeType;

  /** evaluate **/
  void evaluate ( const DomainType & x, RangeType & y ) const
  {
    y[ 0 ] = x[ 0 ] + x[ 1 ];
    y[ 1 ] = 4*x[ 0 ] + x[ 1 ]*x[ 1 ];
  }

  /** jacobian **/
  void jacobian ( const BaseType::DomainType & x, JacobianRangeType & j ) const
  {
    j[ 0 ][ 0 ] = 1.;
    j[ 0 ][ 1 ] = 1.;
    j[ 1 ][ 0 ] = 4.;
    j[ 1 ][ 1 ] = 2*x[ 1 ];
  }
};

class F2
: public LinearFunction< 2, F2 >
{
  typedef LinearFunction< 2, F2 > BaseType;

public:
  /** export dimension of domain **/
  static const int dimension = BaseType::dimension;

  /** single coordinate type **/
  typedef BaseType::ctype ctype;

  /** argument type **/
  typedef BaseType::DomainType DomainType;

  /** range type **/
  typedef BaseType::RangeType RangeType;

  /** type of jacobian **/
  typedef BaseType::JacobianRangeType JacobianRangeType;

  explicit F2 ()
  {
    A_[ 0 ][ 0 ] = 2.;
    A_[ 0 ][ 1 ] = 1.;
    A_[ 1 ][ 0 ] = 1.;
    A_[ 1 ][ 1 ] = 2.;
  }

  /** evaluate **/
  void evaluate ( const DomainType & x, RangeType & y ) const
  {
    A_.mv( x, y );
  }

  /** jacobian **/
  void jacobian ( const BaseType::DomainType & x, JacobianRangeType & j ) const
  {
    j = A_;
  }

private:
  JacobianRangeType A_;
};

#endif
