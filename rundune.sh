#!/usr/bin/env bash

REGISTRY=registry.dune-project.org/claus-justus.heine/implementation-of-finite-elements
BASE=implfem2020

if [ "$1" == "latest" ] ; then
  DUNEVERSION="latest"
elif [ "$1" != "" ] ; then
  DUNEVERSION=$1
else
  DUNEVERSION="2.7"
fi
dockerName=$REGISTRY:$DUNEVERSION
CONTAINER=$BASE-$DUNEVERSION

# Is Docker installed?
haveDocker=$(docker -v)
if [ ! $? -eq 0 ];
then
  echo "Docker could not be found on your system."
  echo "Please install docker following instructions for you operating system from"
  echo "https://docs.docker.com."
  exit 1
fi


# check operating system and start docker
if [ $(uname) = "Linux" ] ;
then
  # need to check if docker is called using 'sudo'
  if [ ! "$SUDO_UID" = "" ] ;
  then
    USERID=$SUDO_UID
    USERNAME=$SUDO_USER
  else
    USERID=$(id -u)
    USERNAME=$USER
  fi
  if [ ! "$SUDO_GID" = "" ] ;
  then
    GROUPID=$SUDO_GID
  else
    GROUPID=$(id -g)
  fi

  # now start docker container
  xhost +si:localuser:$USERNAME
  if ! docker container inspect $CONTAINER >& /dev/null
  then
    echo "Starting new container $DUNEVERSION"
    docker run -it --rm --name $CONTAINER -v $PWD:/host -v $CONTAINER:/dunepy \
      -p 8888:8888 \
      -v /tmp/.X11-unix:/tmp/.X11-unix:ro --device /dev/dri \
      -e userId=$USERID -e groupId=$GROUPID --hostname="$BASE" --add-host $BASE:127.0.0.1 $dockerName
  else
    docker start -i $CONTAINER
  fi
  xhost -si:localuser:$USER
elif [ $(uname) = "Darwin" ] ;
then
  echo "on MAC: for X forwarding remember to run"
  echo '    socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"'
  echo "in separate terminal"
  echo ""
  echo "also note that to run this image docker must be configured to have access to at least 4GB of memory"
  echo "this can be set in the 'advanced' tab of the docker toolbar UI; another option is to run"
  echo "docker-machine stop"
  echo "VBoxManage modifyvm default --memory 4096"
  echo "docker-machine start"
  echo "Changing the maximal number of cores can also increase performance:"
  echo "VBoxManage modifyvm default --cpus 4"
  echo ""
  xhost +si:localuser:$USER
  if ! docker container inspect $CONTAINER >& /dev/null
  then
    docker run -it --rm -v $PWD:/host -v $CONTAINER:/dunepy --name $CONTAINER \
      -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
      -e DISPLAY=$(ipconfig getifaddr en0):0 --net=host \
      -e userId=$(id -u) -e groupId=$(id -g) --hostname="$BASE" --add-host $BASE:127.0.0.1 $dockerName
  else
    docker start -i $CONTAINER
  fi
  xhost -si:localuser:$USER
elif [[ $(uname) = "MINGW64_NT-10.0"* ]] ;
then
  # https://dev.to/darksmile92/run-gui-app-in-linux-docker-container-on-windows-host-4kde
  echo "On Windows: for X forwarding you will need an x-seerver app running use for example vcxsrv -"
  echo "start it with \"xlaunch\" and tick \"disable access control\""
  echo ""
  echo "also note that to run this image docker must be configured to have access to at least 4GB of memory"
  echo "this can be set in the 'advanced' tab of the docker menu."
  echo "Changing the maximal number of cores can also increase performance:"
  echo ""
  if ! docker container inspect $CONTAINER >& /dev/null
  then
    if [ -z "$DISPLAY" ] || [ "$DISPLAY" == "needs-to-be-defined" ];
    then
      export DISPLAY=$(ipconfig | grep "IPv4" | head -1 | grep -oE '[^ ]+$'):0
      echo "Setting DISPLAY to $DISPLAY"
    fi
    winpty docker run -it --rm -v "$PWD":/host -v $CONTAINER:/dunepy --name="$CONTAINER" \
      -e DISPLAY=$DISPLAY --privileged \
      -e userId=$(id -u) -e groupId=$(id -g) $dockerName
  else
    winpty docker start -i $CONTAINER
  fi
else
  echo "System not tested on your architecture identified as $uname"
fi
