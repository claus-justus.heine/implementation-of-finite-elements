# <markdowncell>
# # A projection into a finite element space
# The following requires assembly a finite element
# matrix (the mass matrix) and a right hand side.
# We use linear Lagrange shape functions.
#
# So we are looking for the $L^2$ projection
# \begin{align*}
#    u_h(x) = \sum_k u_k\varphi_k(x)
# \end{align*}
# which is the solution of
# \begin{align*}
#   \int_\Omega u_h\varphi_i &= \int_\Omega u\varphi_i, && \text{for all $i$}
# \end{align*}
# We assume that on an element $E$ we have
# \begin{align*}
#   \varphi_{g_E(k)}(x) = \hat\varphi_k(F_E^{-1}(x))
# \end{align*}
# for $k=0,1,2$ and where $g_E$ denotes the local to global dof mapper
# and $F_E$ is the reference mapping.
#
# So we need to compute
# \begin{align*}
#   M^E_{kl} := \int_{\hat{E}} |DF|\hat\varphi_k\hat\varphi_l~, &&
#   b^E_l := \int_E u\varphi_l~,
# \end{align*}
# and distribute these into a global matrix.
# <codecell>
import numpy
import scipy.sparse
import scipy.sparse.linalg
from dune.geometry import quadratureRules, quadratureRule
from dune.grid import cartesianDomain, gridFunction
from dune.alugrid import aluConformGrid

# <markdowncell>
# ## The shape functions
# We use a simple class here to collect all required
# information about the finite element space, i.e.,
# how to evaluate the shape functions on the reference
# element (together with their derivatives). We also
# setup a mapper to attach the degrees of freedom to
# the entities of the grid.
# <codecell>
class LinearLagrangeSpace:
    def __init__(self,view):
        self.localDofs = 3
        self.view   = view
        self.dim    = view.dimension
        layout      = lambda gt: 1 if gt.dim == 0 else 0
        self.mapper = view.mapper(layout)
        self.points = numpy.array( [ [0,0],[1,0],[0,1] ] )
    def evaluateLocal(self, x):
        bary = 1.-x[0]-x[1], x[0], x[1]
        return numpy.array( bary )
    def gradientLocal(self, x):
        bary  = 1.-x[0]-x[1], x[0], x[1]
        dbary = [[-1.,-1],[1.,0.],[0.,1.]]
        return numpy.array( dbary )

# <markdowncell>
# ## The right hand side and matrix assembly
# We need to iterate over the grid, construct the
# local right hand side and the local system matrix.
# After finishing the quadrature loop we store the
# resulting local matrix in a structure provided by
# the Python package scipy. There are many different
# storage structures available - we use the so called
# 'coordinate' (COO) matrix format which requires us
# construct three vectors, one to store the column
# indices, one for the row indices, and one for the
# values. The convention is that entries appearing
# multiple times are summed up - exactly as we need it.
# So after computing the local matrix and right hand side vector
# $M^E$ we store the values $M^E_{kl}$ into the
# values vector $v_{{\rm start}+3l+k} = M^E_{kl}$
# and the associated global indices
# $c_{{\rm start}+3l+k} = g_E(k)$ and
# $r_{{\rm start}+3l+k} = g_E(l)$.
# <codecell>
def assemble(space,force):
    # storage for right hand side
    rhs = numpy.zeros(len(space.mapper))

    # storage for local matrix
    localEntries = space.localDofs
    localMatrix = numpy.zeros([localEntries,localEntries])

    # data structure for global matrix using coordinate (COO) format
    globalEntries = localEntries**2 * space.view.size(0)
    value = numpy.zeros(globalEntries)
    rowIndex, colIndex = numpy.zeros(globalEntries,int), numpy.zeros(globalEntries,int)

    # iterate over grid and assemble right hand side and system matrix
    start = 0
    for e in view.elements:
        geo = e.geometry
        indices = space.mapper(e)
        localMatrix.fill(0)
        for p in quadratureRule(e.type, 4):
            x = p.position
            w = p.weight * geo.integrationElement(x)
            # evaluate the basis function at the quadrature point
            phiVals = space.evaluateLocal(x)
            # assemble the right hand side
            rhs[indices] += w * force(e,x) * phiVals[:]
            # matrix values
            for i in range(localEntries):
                for j in range(localEntries):
                    localMatrix[i,j] += phiVals[i]*phiVals[j] * w
        # store indices and local matrix for COO format
        indices = space.mapper(e)
        for i in range(localEntries):
            for j in range(localEntries):
                entry = start+i*localEntries+j
                value[entry]    = localMatrix[i,j]
                rowIndex[entry] = indices[i]
                colIndex[entry] = indices[j]
        start += localEntries**2

    # convert data structure to compressed row storage (csr)
    matrix = scipy.sparse.coo_matrix((value, (rowIndex, colIndex)),
                         shape=(len(space.mapper),len(space.mapper))).tocsr()
    return rhs,matrix

# <markdowncell>
# ## The main part of the code
# Construct the grid and a grid function for the
# right hand side, compute the projection and plot
# on a sequence of global grid refinements:
#
# First construct the grid
# <codecell>
domain = cartesianDomain([0, 0], [1, 1], [10, 10])
view   = aluConformGrid(domain)
# <markdowncell>
# then the grid function to project
# <codecell>
@gridFunction(view)
def u(p):
    x,y = p
    return numpy.cos(2*numpy.pi*x)*numpy.cos(2*numpy.pi*y)
u.plot(level=3)

# <markdowncell>
# and then do the projection on a series of globally refined grids
# <codecell>
for ref in range(3):
    space  = LinearLagrangeSpace(view)
    print("number of elements:",view.size(0),"number of dofs:",len(space.mapper))

    rhs,matrix = assemble(space, u)
    dofs = scipy.sparse.linalg.spsolve(matrix,rhs)
    @gridFunction(view)
    def uh(e,x):
        indices = space.mapper(e)
        phiVals = space.evaluateLocal(x)
        localDofs = dofs[indices]
        return numpy.dot(localDofs, phiVals)
    uh.plot(level=1)
    view.hierarchicalGrid.globalRefine(2)


# <markdowncell>
# # Some tasks:
#
# 1. Using the code provided in the introduction `gettingstarted.py` script
# compute some error of the projection, i.e., in maximum
# difference between uh and u at the center of each element,
# or some $L^2$ type error over the domain, e.g.,
# \begin{align*}
#   \sqrt{\int_\Omega |u-u_h|^2}
# \end{align*}
# What are the EOCs?
#
# 2. Implement an interpolation (e.g. on the
# `LinearLagrangeSpace` class) and compare the errors of the
# interpolation with the errors/EOCs you computed for the projection.
#
# 3. Add a class with quadratic finite elements and look at the
# errors/EOCs.
#
# 4. Have a look at the errors/EOCs in the derivatives:
# \begin{align*}
#   \sqrt{\int_\Omega |\nabla u-\nabla u_h|^2}
# \end{align*}
# Recall how the local basis functions are defined and use
# the chain rule, the required method on the element's geometry
# is `jacobianInverseTransposed(x)`...
#
# Warning: getting this to work with vectorization is more advanced
# and needs some testing wrt. the shape of the vectors returned by the
# different vectorized methods....
#
# 5. Add a stiffness matrix and solve the Neuman problem:
# \begin{align*}
#   -\Delta u + u &= f, && \text{in } \Omega, \\
#      \nabla u\cdot n &= 0, && \text{on } \partial\Omega,
# \end{align*}
# where $f$ is for example given by
# ```
# @gridFunction(view)
# def forcing(p):
#     return u(p)*(2*(2*numpy.pi)**2+1)
# ```
#
# Solution: this is implemented in `laplaceNeumann.py`
#
# 6. change the PDE, i.e., include varying coefficients...
#
# <markdowncell>
# ## More advanced things to look at:
#
# 1. Implement the right hand side/matrix assembly in C++
# and use the `algorithm` function to call this implementation.
# You can hopefully now use higher grid resolutions...
#
# Solution: this is implemented in `laplaceNeumannCpp.py, assembly.hh`
#
# 2. Add Dirichlet boundary conditions...
#
# Solution: this is implemented in `laplaceDirichlet.py`
# <codecell>
